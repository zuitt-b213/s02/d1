<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>S02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>
		<h1>Repetition Control Structures</h1>

		<h2>While Loop</h2>
		<?php whileLoop(); ?>

		<h2>Do-while Loop</h2>
		<?php doWhileLoop(); ?>

		<h2>For Loop</h2>
		<?php forLoop(); ?>

		<h1>Array Manipulation</h1>

		<h2>Types of Arrays</h2>

		<h3>Simple Array</h3>
		<ul>
			<?php foreach($computerBrands as $brand){ ?>
				<li><?php echo $brand ?></li>
			<?php } ?>
		</ul>

		<h3>Associative Array</h3>
		<ul>
			<?php foreach($gradePeriods as $period => $grade){ ?>
				<li>Grade in <?= $period ?> is <?= $grade ?></li>
			<?php } ?>
		</ul>
	
	</body>
</html>