<?php

// REPETITION CONTROL STRUCTURES

// While Loop


// For Loop
function forLoop(){
	for ($count = 0; $count <= 20; $count++){
		echo $count . '<br/>';
	}
}

// ARRAY MANIPULATION
	// PHP Arrays are declared using square brackets '[]' or array() function.

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); // used before PHP 5.4
$newStudentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; // introduced on PHP 5.4

// Simple Array
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba'];
$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Associative Array
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 90.1, 'thirdGrading' => 94.3, 'fourthGrading' => 89.2];

// NOTE: <?php echo === <?= (shorthand)

?>